/**
 * @projectName web
 * @package com.bridge.web.service.organ.impl
 * @className com.bridge.web.service.organ.impl.UserServiceImpl
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.service.organ.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bridge.web.entity.User;
import com.bridge.web.mapper.UserMapper;
import com.bridge.web.service.impl.BaseEntityService;
import com.bridge.web.service.organ.IUserService;

import tk.mybatis.mapper.common.Mapper;

/**
 * UserServiceImpl
 * @description TODO
 * @author lenovo
 * @date 2019年9月27日 下午4:37:33
 * @version v1.0.0
 */
@Service
public class UserServiceImpl extends BaseEntityService<User, String> implements IUserService {
    
    @Autowired
    private UserMapper mapper;

    /**
     * @see com.bridge.web.service.impl.BaseEntityService#getMapper() 
     */
    @Override
    protected Mapper<User> getMapper() {
        return mapper;
    }

    /**
     * @see com.bridge.web.service.impl.BaseEntityService#getEntityClass() 
     */
    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

}
