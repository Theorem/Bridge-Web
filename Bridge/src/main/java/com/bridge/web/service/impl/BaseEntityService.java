/**
 * @projectName web
 * @package com.bridge.web.service.impl
 * @className com.bridge.web.service.impl.BaseEntityService
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.service.impl;

import com.bridge.web.service.IEntityService;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

/**
 * BaseEntityService
 * @description TODO
 * @author lenovo
 * @date 2019年9月27日 下午4:46:59
 * @version v1.0.0
 */
public abstract class BaseEntityService<T,K> implements IEntityService<T,K> {

    protected abstract Mapper<T> getMapper();
    
    protected abstract Class<T> getEntityClass();
    
    @Override
    public T getById(K key) {
        return getMapper().selectByPrimaryKey(key);
    }
    
    @Override
    public T getByFieldEquals(String name, Object value) {
        Example example = new Example(getEntityClass());
        example.createCriteria().andEqualTo(name, value);
        return getMapper().selectOneByExample(example);
    }
}
