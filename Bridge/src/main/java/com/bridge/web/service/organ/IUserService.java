/**
 * @projectName web
 * @package com.bridge.web.service
 * @className com.bridge.web.service.IUserService
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.service.organ;

import com.bridge.web.entity.User;
import com.bridge.web.service.IEntityService;

/**
 * IUserService
 * @description 用户服务
 * @author lenovo
 * @date 2019年9月27日 下午4:35:44
 * @version v1.0.0
 */
public interface IUserService extends IEntityService<User, String>{

}
