/**
 * @projectName web
 * @package com.bridge.web.service
 * @className com.bridge.web.service.IEntityService
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.service;

/**
 * IEntityService
 * @description TODO
 * @author lenovo
 * @date 2019年9月27日 下午4:46:13
 * @version v1.0.0
 */
public interface IEntityService<T,K> {

    /**
     * IEntityService
     * @description TODO
     * @param key
     * @return
     * @author lenovo
     * @date 2019年9月27日 下午6:00:49
     * @version v1.0.0
     */
    T getById(K key);

    /**
     * IEntityService
     * @description TODO
     * @param name
     * @param value
     * @return
     * @author lenovo
     * @date 2019年9月27日 下午6:01:44
     * @version v1.0.0
     */
    T getByFieldEquals(String name, Object value);

}
