package com.bridge.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.bridge.web.mapper")
@EntityScan("com.bridge.web.entity")
public class WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}

}
