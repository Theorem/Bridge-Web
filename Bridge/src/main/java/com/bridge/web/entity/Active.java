package com.bridge.web.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import tk.mybatis.mapper.annotation.KeySql;

@Table(name = "t_active")
public class Active {
    @Id
    @KeySql(useGeneratedKeys=true)
    private Long id;

    private BigDecimal money;

    @Column(name = "tagId")
    private Integer tagid;

    @Column(name = "likeNum")
    private Long likenum;

    @Column(name = "collectNum")
    private Long collectnum;

    @Column(name = "activeStatus")
    private Byte activestatus;

    /**
     * 活动名
     */
    private String name;

    /**
     * 概要描述
     */
    private String description;

    /**
     * 发布者ID
     */
    @Column(name = "publishUser")
    private String publishuser;

    /**
     * 所属机构ID
     */
    @Column(name = "organId")
    private String organid;

    /**
     * 活动链接
     */
    private String url;

    /**
     * 活动图链接
     */
    @Column(name = "imgUrl")
    private String imgurl;

    /**
     * 过期时间
     */
    @Column(name = "outTime")
    private Date outtime;

    /**
     * 创建时间
     */
    @Column(name = "createTime")
    private Date createtime;

    /**
     * 修改时间
     */
    @Column(name = "updateTime")
    private Date updatetime;

    /**
     * 详细内容
     */
    private String content;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * @return tagId
     */
    public Integer getTagid() {
        return tagid;
    }

    /**
     * @param tagid
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }

    /**
     * @return likeNum
     */
    public Long getLikenum() {
        return likenum;
    }

    /**
     * @param likenum
     */
    public void setLikenum(Long likenum) {
        this.likenum = likenum;
    }

    /**
     * @return collectNum
     */
    public Long getCollectnum() {
        return collectnum;
    }

    /**
     * @param collectnum
     */
    public void setCollectnum(Long collectnum) {
        this.collectnum = collectnum;
    }

    /**
     * @return activeStatus
     */
    public Byte getActivestatus() {
        return activestatus;
    }

    /**
     * @param activestatus
     */
    public void setActivestatus(Byte activestatus) {
        this.activestatus = activestatus;
    }

    /**
     * 获取活动名
     *
     * @return name - 活动名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置活动名
     *
     * @param name 活动名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取概要描述
     *
     * @return description - 概要描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置概要描述
     *
     * @param description 概要描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取发布者ID
     *
     * @return publishUser - 发布者ID
     */
    public String getPublishuser() {
        return publishuser;
    }

    /**
     * 设置发布者ID
     *
     * @param publishuser 发布者ID
     */
    public void setPublishuser(String publishuser) {
        this.publishuser = publishuser;
    }

    /**
     * 获取所属机构ID
     *
     * @return organId - 所属机构ID
     */
    public String getOrganid() {
        return organid;
    }

    /**
     * 设置所属机构ID
     *
     * @param organid 所属机构ID
     */
    public void setOrganid(String organid) {
        this.organid = organid;
    }

    /**
     * 获取活动链接
     *
     * @return url - 活动链接
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置活动链接
     *
     * @param url 活动链接
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取活动图链接
     *
     * @return imgUrl - 活动图链接
     */
    public String getImgurl() {
        return imgurl;
    }

    /**
     * 设置活动图链接
     *
     * @param imgurl 活动图链接
     */
    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    /**
     * 获取过期时间
     *
     * @return outTime - 过期时间
     */
    public Date getOuttime() {
        return outtime;
    }

    /**
     * 设置过期时间
     *
     * @param outtime 过期时间
     */
    public void setOuttime(Date outtime) {
        this.outtime = outtime;
    }

    /**
     * 获取创建时间
     *
     * @return createTime - 创建时间
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * 设置创建时间
     *
     * @param createtime 创建时间
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * 获取修改时间
     *
     * @return updateTime - 修改时间
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * 设置修改时间
     *
     * @param updatetime 修改时间
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    /**
     * 获取详细内容
     *
     * @return content - 详细内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置详细内容
     *
     * @param content 详细内容
     */
    public void setContent(String content) {
        this.content = content;
    }
}