package com.bridge.web.entity;

import java.util.Date;
import javax.persistence.*;

import tk.mybatis.mapper.annotation.KeySql;
import tkmybatis.comm.UuidGenId;

@Table(name = "t_user")
public class User {
    @Id
    @KeySql(genId = UuidGenId.class)
    private String id;

    @Column(name = "nickName")
    private String nickname;

    @Column(name = "loginName")
    private String loginname;

    private String password;

    @Column(name = "organId")
    private String organid;

    private String phone;

    @Column(name = "createTime")
    private Date createtime;

    @Column(name = "updateTime")
    private Date updatetime;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return nickName
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return loginName
     */
    public String getLoginname() {
        return loginname;
    }

    /**
     * @param loginname
     */
    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return organId
     */
    public String getOrganid() {
        return organid;
    }

    /**
     * @param organid
     */
    public void setOrganid(String organid) {
        this.organid = organid;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return createTime
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * @param createtime
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * @return updateTime
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * @param updatetime
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}