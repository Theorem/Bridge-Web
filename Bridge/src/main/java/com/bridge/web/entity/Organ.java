package com.bridge.web.entity;

import java.util.Date;
import javax.persistence.*;

import tk.mybatis.mapper.annotation.KeySql;
import tkmybatis.comm.UuidGenId;

@Table(name = "t_organ")
public class Organ {
    
    @Id
    @KeySql(genId = UuidGenId.class)
    private String id;

    @Column(name = "organName")
    private String organname;

    /**
     * 机构的链接
     */
    @Column(name = "organUrl")
    private String organurl;

    /**
     * 机构状态
     */
    @Column(name = "organStatus")
    private Byte organstatus;

    /**
     * 创建者
     */
    @Column(name = "createUser")
    private String createuser;

    @Column(name = "createTime")
    private Date createtime;

    @Column(name = "updateTime")
    private Date updatetime;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return organName
     */
    public String getOrganname() {
        return organname;
    }

    /**
     * @param organname
     */
    public void setOrganname(String organname) {
        this.organname = organname;
    }

    /**
     * 获取机构的链接
     *
     * @return organUrl - 机构的链接
     */
    public String getOrganurl() {
        return organurl;
    }

    /**
     * 设置机构的链接
     *
     * @param organurl 机构的链接
     */
    public void setOrganurl(String organurl) {
        this.organurl = organurl;
    }

    /**
     * 获取机构状态
     *
     * @return organStatus - 机构状态
     */
    public Byte getOrganstatus() {
        return organstatus;
    }

    /**
     * 设置机构状态
     *
     * @param organstatus 机构状态
     */
    public void setOrganstatus(Byte organstatus) {
        this.organstatus = organstatus;
    }

    /**
     * 获取创建者
     *
     * @return createUser - 创建者
     */
    public String getCreateuser() {
        return createuser;
    }

    /**
     * 设置创建者
     *
     * @param createuser 创建者
     */
    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    /**
     * @return createTime
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * @param createtime
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * @return updateTime
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * @param updatetime
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}