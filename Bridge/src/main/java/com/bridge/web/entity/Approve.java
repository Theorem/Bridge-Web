package com.bridge.web.entity;

import java.util.Date;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tk.mybatis.mapper.annotation.KeySql;

@Table(name = "t_approve")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Approve {
    /**
     * 主键
     */
    @Id
    @KeySql(useGeneratedKeys=true)
    private Long id;

    /**
     * 审核人
     */
    @Column(name = "approveUser")
    private String approveuser;

    /**
     * 活动ID
     */
    @Column(name = "activeId")
    private Long activeid;

    /**
     * 审核状态（0：不通过，1：通过）
     */
    @Column(name = "approveStatus")
    private Short approvestatus;

    @Column(name = "createTime")
    private Date createtime;

    @Column(name = "updateTime")
    private Date updatetime;

    /**
     * 审核内容
     */
    private String content;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取审核人
     *
     * @return approveUser - 审核人
     */
    public String getApproveuser() {
        return approveuser;
    }

    /**
     * 设置审核人
     *
     * @param approveuser 审核人
     */
    public void setApproveuser(String approveuser) {
        this.approveuser = approveuser;
    }

    /**
     * 获取活动ID
     *
     * @return activeId - 活动ID
     */
    public Long getActiveid() {
        return activeid;
    }

    /**
     * 设置活动ID
     *
     * @param activeid 活动ID
     */
    public void setActiveid(Long activeid) {
        this.activeid = activeid;
    }

    /**
     * 获取审核状态（0：不通过，1：通过）
     *
     * @return approveStatus - 审核状态（0：不通过，1：通过）
     */
    public Short getApprovestatus() {
        return approvestatus;
    }

    /**
     * 设置审核状态（0：不通过，1：通过）
     *
     * @param approvestatus 审核状态（0：不通过，1：通过）
     */
    public void setApprovestatus(Short approvestatus) {
        this.approvestatus = approvestatus;
    }

    /**
     * @return createTime
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * @param createtime
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * @return updateTime
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * @param updatetime
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

}