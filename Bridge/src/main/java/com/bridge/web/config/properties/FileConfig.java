/**
 * @projectName web
 * @package com.bridge.web.config.properties
 * @className com.bridge.web.config.properties.FileConfig
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * FileConfig
 * @description 文件相关配置
 * @author YangWenpeng
 * @date 2019年7月28日 下午4:36:49
 * @version v1.0.0
 */
@Component
public class FileConfig {

    @Value("${file.upload.path}")
    private String uploadPath;
    
    @Value("${file.download.url}")
    private String downLoadUrl;

    /**
     * @return the uploadPath
     */
    public String getUploadPath() {
        return uploadPath;
    }

    /**
     * @return the downLoadUrl
     */
    public String getDownLoadUrl() {
        return downLoadUrl;
    }
    
    
}
