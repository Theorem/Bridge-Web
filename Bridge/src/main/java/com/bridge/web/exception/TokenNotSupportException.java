/**
 * @projectName web
 * @package com.bridge.web.exception
 * @className com.bridge.web.exception.TokenNotSupportException
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.exception;

import org.apache.commons.lang.StringUtils;

/**
 * TokenNotSupportException
 * 
 * @description token不正确异常
 * @author YangWenpeng
 * @date 2019年8月1日 上午10:19:18
 * @version v1.0.0
 */
public class TokenNotSupportException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TokenNotSupportException() {
        super(StringUtils.EMPTY);
    }

    public TokenNotSupportException(String msg) {
        super(msg);
    }

}
