/**
 * @projectName web
 * @package com.bridge.web.interceptor
 * @className com.bridge.web.interceptor.TokenInterceptor
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import com.bridge.web.entity.User;
import com.bridge.web.util.TokenUtils;

/**
 * TokenInterceptor
 * 
 * @description Token验证拦截器
 * @author YangWenpeng
 * @date 2019年7月31日 下午2:25:51
 * @version v1.0.0
 */
@Configuration
public class TokenInterceptor implements HandlerInterceptor {

    /**
     * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object) 
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws Exception {
        User token = TokenUtils.getUser(request);
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}
