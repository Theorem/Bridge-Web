package com.bridge.web.mapper;

import com.bridge.web.entity.Active;

import tk.mybatis.mapper.common.Mapper;

public interface ActiveMapper extends Mapper<Active> {
}