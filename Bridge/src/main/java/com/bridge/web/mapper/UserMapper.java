package com.bridge.web.mapper;

import com.bridge.web.entity.User;

import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}