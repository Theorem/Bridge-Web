package com.bridge.web.mapper;

import com.bridge.web.entity.Organ;

import tk.mybatis.mapper.common.Mapper;

public interface OrganMapper extends Mapper<Organ> {
}