package com.bridge.web.mapper;

import com.bridge.web.entity.Approve;

import tk.mybatis.mapper.common.Mapper;

public interface ApproveMapper extends Mapper<Approve> {
}