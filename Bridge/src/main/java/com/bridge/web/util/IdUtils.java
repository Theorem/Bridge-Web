/**
 * @projectName web
 * @package com.bridge.web.util
 * @className com.bridge.web.util.IdUtils
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.util;

import java.util.UUID;

/**
 * IdUtils
 * 
 * @description Id工具
 * @author YangWenpeng
 * @date 2019年7月28日 下午4:04:14
 * @version v1.0.0
 */
public final class IdUtils {
    private IdUtils() {

    }

    public static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
