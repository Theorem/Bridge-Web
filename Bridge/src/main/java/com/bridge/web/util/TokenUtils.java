/**
 * @projectName web
 * @package com.bridge.web.util.token.impl
 * @className com.bridge.web.util.token.impl.SessionTokenUtils
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.bridge.web.entity.User;
import com.bridge.web.exception.TokenNotSupportException;

/**
 * SessionTokenUtils
 * 
 * @description 基于Session的凭证工具
 * @author YangWenpeng
 * @date 2019年7月31日 下午2:22:24
 * @version v1.0.0
 */
public final class TokenUtils {

    private TokenUtils() {}

    public static String getToken(HttpServletRequest request) {
        return (String)request.getSession().getAttribute("token");
    }

    public static void setToken(HttpServletRequest request, String token) {
        request.getSession().setAttribute("token", token);
    }

    public static String getToken() {
        HttpServletRequest request
            = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
        return getToken(request);
    }

    public static void setToken(String token) {
        HttpServletRequest request
            = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
        setToken(request, token);
    }

    public static String getUserId() throws TokenNotSupportException {
        String token = getToken();
        String[] tokenArray = token.split("|");
        if (tokenArray.length != 3) {
            throw new TokenNotSupportException();
        }
        return tokenArray[2];
    }

    public static Object getUser() {
        HttpServletRequest request
        = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
        return getUser(request);
    }
    
    public static User getUser(HttpServletRequest request) {
        return (User)request.getSession().getAttribute("user");
    }

    /**
     * TokenUtils
     * @description TODO
     * @param user
     * @author lenovo
     * @date 2019年9月27日 下午6:09:14
     * @version v1.0.0
     */
    public static String setUser(User user) {
        HttpServletRequest request
        = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
        return setUser(request,user);
    }

    /**
     * TokenUtils
     * @description TODO
     * @param request
     * @param user
     * @return
     * @author lenovo
     * @date 2019年9月27日 下午6:10:52
     * @version v1.0.0
     */
    private static String setUser(HttpServletRequest request, User user) {
        request.getSession().setAttribute("user", user);
        return user.getId();
    }

}
