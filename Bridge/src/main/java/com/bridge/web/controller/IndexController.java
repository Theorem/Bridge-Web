/**
 * @projectName Bridge
 * @package com.Bridge.web.controller
 * @className com.Bridge.web.controller.IndexController
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * IndexController
 * @description IndexController
 * @author YangWenpeng
 * @date 2019年6月10日 下午6:20:22
 * @version v1.0.0
 */
@Controller
public class IndexController {

    @GetMapping("index")
    public String index() {
        return "index";
    }
}
