/**
 * @projectName web
 * @package com.bridge.web.controller
 * @className com.bridge.web.controller.UploadController
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.controller;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bridge.web.config.properties.FileConfig;
import com.bridge.web.util.IdUtils;

/**
 * UploadController
 * 
 * @description 上传控制器
 * @author YangWenepng
 * @date 2019年7月28日 下午3:54:43
 * @version v1.0.0
 */
@RestController
@RequestMapping("v1/upload")
public class UploadController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private FileConfig fileConfig;

    private static final String FILE_NAME_PREFIX = "file_";

    @PostMapping
    public String uploadFile(MultipartFile file) {
        String fileName = FILE_NAME_PREFIX + IdUtils.getUuid();
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), new File(fileConfig.getUploadPath(), fileName));
        } catch (IOException e) {
            log.error("上传文件失败，文件名："+FilenameUtils.getName(file.getOriginalFilename()),e );
        }
        return fileConfig.getDownLoadUrl().replace("#[fileName]", fileName);
    }
}
