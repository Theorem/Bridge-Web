/**
 * @projectName web
 * @package com.bridge.web.controller
 * @className com.bridge.web.controller.UserController
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.controller;

import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bridge.web.entity.User;
import com.bridge.web.service.organ.IUserService;
import com.bridge.web.util.MD5Util;
import com.bridge.web.util.TokenUtils;

/**
 * UserController
 * @description TODO
 * @author lenovo
 * @date 2019年9月27日 下午4:13:33
 * @version v1.0.0
 */
@RestController
@RequestMapping("api/user")
public class UserController {
    
    @Autowired
    private IUserService userService;
    
    @PostMapping("login/v1")
    public ModelAndView login(@RequestParam String name,@RequestParam String key,ModelAndView modelAndView) {
        User user = userService.getByFieldEquals("loginName", name);
        if(!MD5Util.validPassword(key, user.getPassword())){
            modelAndView.addObject("name", name);
            modelAndView.addObject("error", "用户名或密码错误");
            modelAndView.setViewName("login");
            return modelAndView;
        }
        TokenUtils.setUser(user);
        modelAndView.setViewName("index");
        return modelAndView;
    }

}
