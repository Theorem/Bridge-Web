/**
 * @projectName web
 * @package com.bridge.web.controller
 * @className com.bridge.web.controller.DownloadController
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bridge.web.config.properties.FileConfig;

/**
 * DownloadController
 * 
 * @description 下载
 * @author YangWenpeng
 * @date 2019年7月28日 下午4:35:29
 * @version v1.0.0
 */
@RestController
@RequestMapping("v1/download")
public class DownloadController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private FileConfig fileConfig;

    @GetMapping("{fileName}")
    public void baseDownload(@PathVariable("fileName") String fileName, HttpServletResponse response)
        throws IOException {
        File file = new File(fileConfig.getUploadPath(), fileName);
        FileUtils.copyFile(file, response.getOutputStream());
    }
}
