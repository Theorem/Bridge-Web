/**
 * @projectName web
 * @package com.bridge.web.controller
 * @className com.bridge.web.controller.ExceptionHandler
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bridge.web.util.IPHelper;

/**
 * ExceptionHandler
 * @description 异常处理器
 * @author YangWenpeng
 * @date 2019年7月28日 下午5:02:23
 * @version v1.0.0
 */
@ControllerAdvice
@RestController
public class BaseExceptionHandler {
    
    private Logger log = LoggerFactory.getLogger(getClass());
    
    @GetMapping
    public void testFailedException() {
        throw new RuntimeException();
    }
    
    @ExceptionHandler(value=MissingServletRequestParameterException.class)
    public Object missingServletRequestParameterException(Throwable throwable,HttpServletResponse response,HttpServletRequest request) {
        
        log.warn("==============警告！！！一个请求参数错误==============");
        log.warn("请求源IP：{}",IPHelper.getIpAddr(request));
        log.warn("请求路径：{}",request.getRequestURI());
        log.warn("请求参数:{}",request.getParameterMap());
        response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        return "服务器出错";
    }
    
    @ExceptionHandler(value=Exception.class)
    public Object exception(Throwable throwable,HttpServletResponse response) {
        log.error("服务器出现错误",throwable);
        response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        return "服务器出错";
    }

}
