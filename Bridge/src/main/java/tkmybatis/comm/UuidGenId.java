/**
 * @projectName web
 * @package tkmybatis.comm
 * @className tkmybatis.comm.UuidGenId
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package tkmybatis.comm;

import java.util.UUID;

import tk.mybatis.mapper.genid.GenId;

/**
 * UuidGenId
 * 
 * @description 主键生成策略
 * @author YangWenpeng
 * @date 2019年7月12日 下午4:36:44
 * @version v1.0.0
 */
public class UuidGenId implements GenId<Object> {

    /**
     * @see tk.mybatis.mapper.genid.GenId#genId(java.lang.String, java.lang.String)
     */
    @Override
    public Object genId(String table, String column) {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
