package tkmybatis.comm;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author lilin
 * @date 2018/7/13
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
  // do nothing
}
