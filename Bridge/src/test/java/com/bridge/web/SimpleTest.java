/**
 * @projectName web
 * @package com.Bridge.web
 * @className com.Bridge.web.SimpleTest
 * @copyright Copyright 2019 Thuisoft, Inc. All rights reserved.
 */
package com.bridge.web;

import org.junit.Test;

/**
 * SimpleTest
 * @description TODO
 * @author lenovo
 * @date 2019年8月1日 下午6:22:56
 * @version TODO
 */
public class SimpleTest {

    @Test
    public void test() {
        int i = 0;
        test2(i);
    }
    
    private void test2(Object i) {
        System.out.print(i instanceof Integer);
        System.out.println(i.getClass());
    }
}
