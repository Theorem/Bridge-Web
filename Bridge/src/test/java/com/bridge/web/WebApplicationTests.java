package com.bridge.web;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bridge.web.entity.User;
import com.bridge.web.mapper.ApproveMapper;
import com.bridge.web.mapper.UserMapper;

@SpringBootTest
class WebApplicationTests {

    @Autowired
    private ApproveMapper mapper;
    @Autowired
    private UserMapper userMapper;
    
	@Test
	void contextLoads() {
	    try {
	    User user = new User();
	    user.setLoginname("test");
	    user.setNickname("test");
	    user.setOrganid("123");
	    user.setPassword("123");
	    userMapper.insertSelective(user);
	    }catch(Exception e) {
	        e.printStackTrace();
	    }
	}

}
